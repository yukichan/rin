CC=avr-gcc
CFLAGS=-Os -Wall

HEX=avr-objcopy

DEV 		= attiny85
CLOCK		= 1000000
PROGRAMMER	= usbasp
PORT		= usb
BAUD		= 19200
FILENAME	= rin
COMPILE		= $(CC) $(CFLAGS) -DF_CPU=$(CLOCK) -mmcu=$(DEV)

all: usb clean build upload

usb:
	ls /dev

build:
	$(COMPILE) -c $(FILENAME).c -o $(FILENAME).o
	$(COMPILE) -o $(FILENAME).elf $(FILENAME).o
	avr-objcopy -j .text -j .data -O ihex $(FILENAME).elf $(FILENAME).hex
	avr-size --format=avr --mcu=$(DEV) $(FILENAME).elf

upload:
	avrdude -v -p $(DEV) -c $(PROGRAMMER) -b $(BAUD) -U flash:w:$(FILENAME).hex:i

clean: 
	rm -f $(FILENAME).o
	rm -f $(FILENAME).elf
	rm -f $(FILENAME).hex

test.o:
	$(CC) 
