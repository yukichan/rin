#include <avr/io.h>
#include <util/delay.h>

#define MIN_BRIGHTNESS 0xA0
#define MAX_BRIGHTNESS 0xFF
#define FADE_SPEED 10

#define PWM_PIN_0 PB0
#define PWM_PIN_1 PB1
#define INPUT_PIN PB2

signed char delta;
int duty_cycle;


void setup_ports(void);
void setup(void);

int main(void) {
  setup();
  while(1) {
    //Handle any fading
    duty_cycle += delta;
    duty_cycle = (duty_cycle < MIN_BRIGHTNESS) ? MIN_BRIGHTNESS : duty_cycle;
    duty_cycle = (duty_cycle > MAX_BRIGHTNESS) ? MAX_BRIGHTNESS : duty_cycle;
    OCR0A = duty_cycle;
    //Change delta to match input pin state
    delta = (PINB & (1 << INPUT_PIN)) == 0 ? FADE_SPEED : 0-FADE_SPEED;
    _delay_ms(10);
  }
}

void setup(void) {
  setup_ports();
  //Initialize LED brightnesses
  duty_cycle = OCR0A = OCR0B = MIN_BRIGHTNESS;
  //Initialize brihtness delta
  delta = -FADE_SPEED;
}


void setup_ports(void) {
  //Setup ports 0 and 1 as output
  DDRB = ((1 << PWM_PIN_0) | (1 << PWM_PIN_1));
  //Setup port 2 as input
  DDRB |= ((0 << INPUT_PIN));
  //Add pullup to port 2;
  PORTB |= (1 << INPUT_PIN);

  //Setup PWM
  //Enable Fast PWM with non-inverted operation on PB0-1. Run PWM at full CPU frequency
  TCCR0A = ((2 << COM0A0) | (2 << COM0B0) | (3 << WGM00));
  TCCR0B = ((0 << FOC0A) | (0 << FOC0B) | (0 << WGM02) | (3 << CS00));
}

